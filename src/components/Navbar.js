import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    marginRight: 30,
  },
  navlink: {
    color: "white",
    textDecoration: "none",
  },
}));

export default function App() {
  const classes = useStyles();
  const navigate = useNavigate();

  const clearLocalStorage = () => {
    localStorage.clear();
    navigate("/");
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Link className={classes.navlink} to="/">
            <Typography variant="h4" className={classes.title}>
              Voting System
            </Typography>
          </Link>

          {localStorage.getItem("nationalId") ? (
            <>
              <Link className={classes.navlink} to="/">
                <Typography variant="h7" className={classes.title}>
                  Blockchain
                </Typography>
              </Link>
              <Link className={classes.navlink} to="/create">
                <Typography variant="h7" className={classes.title}>
                  Vote
                </Typography>
              </Link>
              <Link className={classes.navlink} to="/results">
                <Typography variant="h7" className={classes.title}>
                  Results
                </Typography>
              </Link>
              <div className={classes.navlink} onClick={clearLocalStorage}>
                <Typography variant="h7" className={classes.title}>
                  Logout
                </Typography>
              </div>
            </>
          ) : null}
        </Toolbar>
      </AppBar>
    </div>
  );
}
