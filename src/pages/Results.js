import React, { useEffect, useState } from "react";
import "../App.css";

export default function BlocksList() {
  const [blocks, setBlocks] = useState([]);
  useEffect(() => {
    BlocksGet();
  }, []);

  const BlocksGet = () => {
    fetch("http://127.0.0.1:8080/results/")
      .then((res) => res.json())
      .then((result) => {
        setBlocks(result);
      });
  };

  console.log("blocks");
  console.log(blocks);

  console.log("blocks[Rafael Nadal]");

  console.log(blocks["Rafael Nadal"]);

  const rn = blocks["Rafael Nadal"];
  const nd = blocks["Novak Djokovic"];
  const n = blocks["None"];

  console.log("rn");
  console.log(rn);
  console.log(typeof rn);

  const data = [
    { candidate: "Rafael Nadal", expense: rn },
    { candidate: "Novak Djokovic", expense: nd },
    { candidate: "None", expense: n },
  ];

  console.log("data");
  console.log(data);

  const Chart = ({ children, width, height }) => (
    <svg
      viewBox={`0 0 ${width} ${height}`}
      width="100%"
      height="70%"
      preserveAspectRatio="xMidYMax meet"
    >
      {children}
    </svg>
  );

  const Bar = ({ x, y, width, height, expenseName, highestExpense }) => (
    <>
      <rect
        x={x}
        y={y}
        width={width}
        height={height}
        fill={highestExpense === height ? `purple` : `black`}
      />
      <text x={x + width / 3} y={y - 5}>
        {highestExpense === height
          ? `${expenseName}: ${height}`
          : `${expenseName}:${height}`}
      </text>
    </>
  );

  // const [expensesData, setExpensesData] = useState(data);
  const maxExpense = 200;
  const chartHeight = 20;
  const barWidth = 50;
  const barMargin = 30;
  const numberofBars = data.length;
  let width = numberofBars * (barWidth + barMargin);

  // Calculate highest expense for the month
  const calculateHighestExpense = (data) =>
    data.reduce((acc, cur) => {
      const { expense } = cur;
      return expense > acc ? expense : acc;
    }, 0);

  const [highestExpense, setHighestExpense] = useState(
    calculateHighestExpense(data)
  );

  useEffect(() => {
    console.log(JSON.stringify(data));
    console.log(highestExpense);
  });

  return (
    <div className="bar-chart">
      <Chart height={chartHeight} width={width}>
        {data.map((d, index) => {
          console.log("d.expense");

          console.log(d);
          const barHeight = d.expense;

          console.log("data11111111111111");
          console.log(d);

          console.log("barWidth");
          console.log(barWidth);

          console.log("barMargin");
          console.log(barMargin);

          console.log("chartHeight");
          console.log(chartHeight);

          console.log("barHeight");
          console.log(barHeight);
          return (
            <Bar
              key={d.candidate}
              x={index * (barWidth + barMargin)}
              y={chartHeight - barHeight}
              width={barWidth}
              height={barHeight}
              expenseName={d.candidate}
              highestExpense={highestExpense}
            />
          );
        })}
      </Chart>
    </div>
  );
}
