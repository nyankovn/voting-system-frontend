/* App.js */
import React, { useEffect, useState } from "react";
import CanvasJSReact from "../assets/canvasjs.react";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
export default function Chart() {
  const [blocks, setBlocks] = useState([]);
  useEffect(() => {
    BlocksGet();
  }, []);

  const BlocksGet = () => {
    fetch("http://127.0.0.1:8080/results/")
      .then((res) => res.json())
      .then((result) => {
        setBlocks(result);
      });
  };

  const rn = blocks["Rafael Nadal"];
  const nd = blocks["Novak Djokovic"];
  const n = blocks["None"];

  const data = [
    { candidate: "Rafael Nadal", expense: rn },
    { candidate: "Novak Djokovic", expense: nd },
    { candidate: "None", expense: n },
  ];

  const options = {
    title: {
      text: "Vote results",
    },
    data: [
      {
        // Change type to "doughnut", "line", "splineArea", etc.
        type: "column",
        dataPoints: [
          { label: "Rafael Nadal", y: rn },
          { label: "Novak Djokovic", y: nd },
          { label: "None", y: n },
        ],
      },
    ],
  };
  return (
    <div>
      <CanvasJSChart
        options={options}
        /* onRef={ref => this.chart = ref} */
      />
      {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
    </div>
  );
}
