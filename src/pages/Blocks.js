import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  container: {
    marginTop: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
}));

export default function BlocksList() {
  const classes = useStyles();

  const [blocks, setBlocks] = useState([]);
  useEffect(() => {
    BlocksGet();
  }, []);

  const BlocksGet = () => {
    fetch("http://127.0.0.1:8080")
      .then((res) => res.json())
      .then((result) => {
        setBlocks(result);
      });
  };

  return (
    <div className={classes.root}>
      <Container className={classes.container} maxWidth="lg">
        <Paper className={classes.paper}>
          <Box display="flex">
            <Box flexGrow={1}>
              <Typography
                component="h2"
                variant="h6"
                color="primary"
                gutterBottom
              >
                BLOCKCHAIN
              </Typography>
            </Box>
          </Box>
          <TableContainer component={Paper}>
            <Table className={classes.time_stamp} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="right">ID</TableCell>
                  <TableCell align="center">Timestamp</TableCell>
                  <TableCell align="left">Candidate</TableCell>
                  <TableCell align="left">Voter Identity</TableCell>
                  <TableCell align="left">Hash</TableCell>
                  <TableCell align="left">Previous Hash</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {blocks.map((block) => (
                  <TableRow key={block.index}>
                    <TableCell align="right">{block.index}</TableCell>
                    <TableCell align="left">{block.time_stamp}</TableCell>
                    <TableCell align="left">{block.vote.candidate}</TableCell>
                    <TableCell align="left">
                      {block.vote.voter_identity}
                    </TableCell>
                    <TableCell align="left">{block.hash}</TableCell>
                    <TableCell align="left">{block.prev_hash}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </Container>
    </div>
  );
}
