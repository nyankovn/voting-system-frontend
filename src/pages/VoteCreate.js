import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";

import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },

  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function VoteCreate() {
  const classes = useStyles();

  const handleSubmit = (event) => {
    event.preventDefault();
    var data = {
      vote_candidate: candidate,
      voter_identity: {
        national_id: nationalId,
        name: fname + " " + lname,
      },
    };
    fetch("http://127.0.0.1:8080", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }).then((res) => {
      if (res.ok) {
        alert("OK");
      } else {
        alert("You have already voted!");
      }
    });
  };

  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [nationalId, setNationalId] = useState("");
  const [candidate, setCandidate] = useState("");

  useEffect(() => {
    setFname(localStorage.getItem("fname"));
    setLname(localStorage.getItem("lname"));
    setNationalId(localStorage.getItem("nationalId"));
  }, []);

  const handleChange = (event) => {
    setCandidate(event.target.value);
  };
  return (
    <Container maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Who is the best tenis player?
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="First Name"
                disabled
                value={fname}
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                disabled
                value={lname}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="nationalId"
                label="National Id"
                disabled
                value={nationalId}
              />
            </Grid>

            <Grid item xs={12}>
              <InputLabel id="demo-simple-select-label">Candidate</InputLabel>
              <Select
                variant="outlined"
                required
                fullWidth
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={candidate}
                label="Candidate"
                onChange={handleChange}
              >
                <MenuItem value={"Rafael Nadal"}>Rafael Nadal</MenuItem>
                <MenuItem value={"Novak Djokovic"}>Novak Djokovic</MenuItem>
                <MenuItem value={"None"}>None</MenuItem>
              </Select>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Vote
          </Button>
        </form>
      </div>
    </Container>
  );
}
