import React, { useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Navbar from "./components/Navbar";
import Blocks from "./pages/Blocks";
import Chart from "./pages/Chart";
import Auth from "./pages/Auth";

import VoteCreate from "./pages/VoteCreate";

export default function App() {
  return (
    <Router>
      <div>
        <Navbar />
        {localStorage.getItem("nationalId") ? (
          <Routes>
            <Route path="/" element={<Blocks />} />
            <Route path="/create" element={<VoteCreate />} />
            <Route path="/results" element={<Chart />} />
            <Route path="/auth" element={<Auth />} />
          </Routes>
        ) : (
          <Routes>
            <Route path="/" element={<Auth />} />
          </Routes>
        )}
      </div>
    </Router>
  );
}
